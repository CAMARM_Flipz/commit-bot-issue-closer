# Intro:

Did you ever think that close your issues with a commit message will be very cool and comfortable !<br>
Me, yes.

# Use:
To use this bot at home, just [run an instance](#run-an-instance) and modify file git_urls.list with your repositories, separated by a `,`.
<br>To make it works, commit message must be looking like this: 
```
%issue:#20%
Resolve bug on home button when...
```
This will search issue #20 and close it if exist ! Very cool, No ?<br>
Please note that for project they are inside groups you must provide group token !

# Run an instance:

1. 
```shell
pip install -r requirements.txt
```
2. Modify bot.ini file with your values.
3. Modify git_urls.list with your repositories, separated by a `,` (only between).
4. Start the script 
```shell
python3 main.py --config bot.ini
```
